#!/bin/bash

# Counts how many tweets there are in the given file.
echo "Amount of tweets:" 
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l


# Counts how many unique tweets there are.
echo "Amount of unique tweets:" 
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | wc -l


# Counts how many retweets there are (out of the unique tweets).
echo "Amount of Retweets:" 
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -e ^'RT' | wc -l 


# Shows the first 20 unique tweets that are not retweets.
echo "First 20 unique tweets:"
zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | awk '!x[$0]++' | grep -v -e ^'RT' | head -20


