# README #

This repository contains a  shell script called 'sample-tweets.sh' that accesses a Dutch Twitter corpus from Rijksuniversiteit Groningen. From this corpus it uses the twitter file from 1 March 2017 12:00. It also uses the tool 'tweet2tab'.

The shell script counts:

- how many tweets there are in the file.

- how many unique tweets there are in the file.

- how many retweets there are in the file (out of the unique tweets).

After that it shows the first 20 unique tweets in the file that are not retweets.